# README #

This README documents the steps required to run this project.

### What is this repository for? ###

* This is a project to demonstrate various testing methods.
* This version is unreleased, and this project will never be released.


### How do I get set up? ###

0. Setup virtualenv.
1. Run the tests:
```
./run_tests.sh
```

### Contribution guidelines ###

* Submit pull requests.
* All changes must be accompanied by working tests. No exceptions.


### Who do I talk to? ###

* Talk to Brian May.
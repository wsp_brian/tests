""" This file provides the ABC objects for Special. """

def _diff(old_values, values):
    result = {}
    for key in values.keys():
        if key not in old_values or old_values[key] != values[key]:
            result[key] = values[key]
    return result

def _send_to_server(diff):
    # pretend this writes values to the server
    print(diff)


class AbcCache:
    def __init__(self, record=None):
        if record is None:
            self._record = {}
        else:
            self._record = dict(record)

    def get(self, key):
        return self._record.get(key)

    @property
    def record(self):
        return self._record

    @record.setter
    def record(self, value):
        self._record = dict(value)

class Abc:
    def __init__(self, id, cache=None):
        self.values = {
            'id': id,
        }
        if cache is None:
            self._cache = AbcCache()
        else:
            self._cache = cache


    def save(self):
        old_id = self._cache.get('id')
        if old_id is not None:
            # if object already created, we cannot change the id
            assert old_id == self.values['id']
        result = _diff(self._cache.record, self.values)
        _send_to_server(result)
        self._cache.record = self.values

""" This tests the json module for special. """

import special.abc


def test_diff():
    old_values = {
        'a': 'hello',
        'b': 'world',
        'c': True,
        'd': None,
    }

    values = {
        'a': 'goodbye',
        'b': 'world',
        'c': True,
        'd': None,
        'e': None,
    }

    result = special.abc._diff(old_values, values)

    assert result == {
        'a': 'goodbye',
        'e': None,
    }

def test_save_first(mocker):
    mock_send_to_server = mocker.patch('special.abc._send_to_server')
    cache = special.abc.AbcCache()

    # Test initial values
    my_object = special.abc.Abc(1, cache=cache)
    my_object.values['a'] = 'hello'
    my_object.values['b'] = 'world'
    my_object.values['c'] = True
    my_object.values['d'] = None
    my_object.save()

    expected_result = {
        'id': 1,
        'a': 'hello',
        'b': 'world',
        'c': True,
        'd': None,
    }

    mock_send_to_server.assert_called_with(expected_result)
    assert cache.record == expected_result


def test_save_second(mocker):
    mock_send_to_server = mocker.patch('special.abc._send_to_server')
    cache = special.abc.AbcCache({
        'id': 1,
        'a': 'hello',
        'b': 'world',
        'c': True,
        'd': None,
    })

    my_object = special.abc.Abc(1, cache=cache)
    my_object.values = dict(cache.record)
    my_object.values['a'] = 'goodbye'
    my_object.values['b'] = 'world'
    my_object.values['c'] = True
    my_object.values['d'] = None
    my_object.save()

    mock_send_to_server.assert_called_with({
        'a': 'goodbye',
    })

    assert cache.record == {
        'id': 1,
        'a': 'goodbye',
        'b': 'world',
        'c': True,
        'd': None,
    }
